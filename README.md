Experiment Spring Boot - `@ConfigurationProperties` aka Type-safe Configuration Properties
==========================================================================================

[Type-safe Configuration Properties](https://docs.spring.io/spring-boot/docs/current/reference/html/spring-boot-features.html#boot-features-external-config-typesafe-configuration-properties)
> Using the @Value("${property}") annotation to inject configuration properties can sometimes be cumbersome, especially if you are working with multiple properties or your data is hierarchical in nature.
> Spring Boot provides an alternative method of working with properties that lets strongly typed beans govern and validate the configuration of your application.

Spring Boot 2.2
---------------
This project use Spring Boot 2.2 (2.2.7), so it can use the some Spring Boot 2.2 simplification when it comes to `@ConfigurationProperties`.

https://github.com/spring-projects/spring-boot/wiki/Spring-Boot-2.2-Release-Notes#configurationproperties-scanning.
As, of Spring Boot 2.2, Spring finds and registers `@ConfigurationProperties` classes via classpath scanning.
Therefore, there is no need to annotate such classes with `@Component` (and other meta-annotations like `@Service` or `@Configuration`).
To enable this, add `@ConfigurationPropertiesScan`.
See [@ConfigurationProperties scanning](https://github.com/spring-projects/spring-boot/wiki/Spring-Boot-2.2-Release-Notes#configurationproperties-scanning)

constructor-based binding / [Immutable `@ConfigurationProperties` binding](https://github.com/spring-projects/spring-boot/wiki/Spring-Boot-2.2-Release-Notes#immutable-configurationproperties-binding)
> Constructor-based binding can be enabled by annotating a `@ConfigurationProperties` class or one of its constructors with `@ConstructorBinding`.
> Annotations such as `@DefaultValue` and `@DateTimeFormat` can now be used on constructor parameters that are provided by configuration property binding.
> Please see the [relevant section](https://docs.spring.io/spring-boot/docs/2.2.0.RELEASE/reference/html/spring-boot-features.html#boot-features-external-config-constructor-binding) of the reference documentation for further details.

So with Spring Boot 2.2 there are two different styles:
* Add `@ConfigurationPropertiesScan` to the spring-config class and add `@ConfigurationProperties` and maybe `@ConstructorBinding` at the configuration class
* Add `@EnableConfigurationProperties` to the spring-config class and add `@Component` and `@ConfigurationProperties` to the configuration class.
  But this Approach does not support connstructor binding.
  
But *`@ConfigurationPropertiesScan`*, `@ConfigurationProperties` *and `@Component`* will be a mess, because it try to instanciate the bean twice.
If you have `@ConfigurationPropertiesScan`, `@ConfigurationProperties`, `@ConstructorBinding` and `@Component` you will get a missleading exception:
> org.springframework.beans.factory.BeanCreationException: Error creating bean with name 'configurationA': @EnableConfigurationProperties or @ConfigurationPropertiesScan must be used to add @ConstructorBinding type de.humanfork.experiment.springboot.configurationproperties.ConfigurationA

spring-configuration-metadata.json
----------------------------------

The Java Processor `spring-boot-configuration-processor` will create a file `target\classes\META-INF\spring-configuration-metadata.json` that contains a description of all configuration properties. 
This is for example used by the IDE for completion and hints.

```
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-configuration-processor</artifactId>
	<optional>true</optional>
</dependency>
```

Upper Case and Relaxed Binding
------------------------------

> ###[Relaxed Binding](https://docs.spring.io/spring-boot/docs/2.1.x/reference/html/boot-features-external-config.html#boot-features-external-config-relaxed-binding)
>
> Spring Boot uses some relaxed rules for binding Environment properties to `@ConfigurationProperties` beans, so there does not need to be an exact match between the Environment property name and the bean property name.

Important:
- This is only for `@ConfigurationProperties` but not for `@Value` annotations
- The property name defined in the code `@ConfigurationProperties(prefix="acme.my-project.person")` MUST USE the `-`-style (Kebab case)!
  The property name in the Environment properties can be for example Standard camel case syntax: `acme.myProject.person`

Binding collection types
------------------------

### Maps
> When binding to Map properties, if the key contains anything other than lowercase alpha-numeric characters or -, you need to use the bracket notation so that the original value is preserved.
> If the key is not surrounded by [], any characters that are not alpha-numeric or - are removed.
> For example, consider binding the following properties to a Map:
> ```
> acme.map.[/key1]=value1
> acme.map.[/key2]=value2
> acme.map./key3=value3    #without []
> ```
> The properties above will bind to a Map with `/key1`, `/key2` and `key3` as the keys in the map.

Lombok
------
This project use [Project Lombok](https://projectlombok.org/) therefore you need to have Lombok installed

### Lombok - Eclipse - Overlapping text edits bug
There is one problem with Eclipse: The cleanup setting with "Code Style / Use parentheses in expressions".
If it is enabled, then Eclipse cleanup will fail for `@Data` annotated classes:
> An unexpected exception occured while performing the refactoring.
>
> Overlappping text edits
https://bugs.eclipse.org/bugs/show_bug.cgi?id=535536

Resources
---------

- Spring Boot GitHub Doku [Spring Boot Configuration Binding](https://github.com/spring-projects/spring-boot/wiki/Spring-Boot-Configuration-Binding)