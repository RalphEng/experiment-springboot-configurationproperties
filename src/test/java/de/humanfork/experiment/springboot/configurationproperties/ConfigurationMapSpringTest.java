package de.humanfork.experiment.springboot.configurationproperties;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import de.humanfork.experiment.springboot.configurationproperties.ConfigurationMap.MapItem;

@SpringBootTest
public class ConfigurationMapSpringTest {

    @Autowired
    private ConfigurationMap configurationMap;
    
    @Test
    public void test() {
        assertThat(this.configurationMap.getMap()).hasSize(4);
        assertThat(this.configurationMap.getMap()).containsKeys("father", "mother", "son", "daughter");
        assertThat(this.configurationMap.getMap()).containsEntry("father", new MapItem("Ralph", "Engelmann"));
    }
}
