package de.humanfork.experiment.springboot.configurationproperties;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * Test {@code App}.
 */

@SpringBootTest
public class AppTest {

    @Test
    void contextLoads() {
    }
}
