package de.humanfork.experiment.springboot.configurationproperties;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class ConfigurationASpringTest {

    @Autowired
    private ConfigurationA configurationA;
    
    @Test
    public void test() {
        assertThat(this.configurationA.getSomeStringProperty()).isEqualTo("myStringA");
    }
}
