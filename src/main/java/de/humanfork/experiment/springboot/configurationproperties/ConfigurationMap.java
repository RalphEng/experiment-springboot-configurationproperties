package de.humanfork.experiment.springboot.configurationproperties;

import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;

import lombok.Data;

/**
 * Example for an configuration that contains a map. 
 */
@ConfigurationProperties(prefix = "config-map")
@ConstructorBinding
@Data
public class ConfigurationMap {

    /** A map - the configuration define keys, values, as well as number of occurences. */
    private final Map<String, MapItem> map;

    @Data
    public static class MapItem {
        
        /**
         * The forname.
         */
        private final String firstname;
        
        /**
         * The familyname
         */
        private final String lastname;        
    }
}
