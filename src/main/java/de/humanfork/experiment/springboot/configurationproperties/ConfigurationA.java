package de.humanfork.experiment.springboot.configurationproperties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@ConfigurationProperties(prefix = "my-demo-config-a")
@ConstructorBinding
@RequiredArgsConstructor
@Getter
public class ConfigurationA {

    private final String someStringProperty;
    
    private final int someIntegerProperty;
    
    
        
}
